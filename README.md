# 4*4 Ultimate Tic-Tac-Toe A.I. Bot

* Krishn Bera
* Tirth Maniar 

An A.I. Bot in Python for playing 4*4 Ultimate Tic-Tac-Toe using mini-max search techniques, alpha-beta pruning and custom heuristic functions. The bot is nicknamed "Invictus" for its no-lose game play in the specified game constraints.

To run the AI bot, type in the terminal: `python simulator.py 2`

Player 1 (X) is the bot -'INVICTUS' and Player 2 (O) is the opponent which is also an A.I. bot. 
Each move is printed on the screen and the game result will be summarized at the end.


## Search Algorithm
- Min-Max algorithm with alpha-beta pruning with progressive deepening
- Min depth starting with 3 and goes up till depth 5-6 in middle game and upto 12 in end game
- Time limit for each move: 15 sec
- Transposition Table for optimization


## Heuristics
The board is the entire (16x16) game board. A block is a (4x4) subgrid. A cell is a single (1x1) cell.

#### Block Score
- Calculate number of rows, columns and diagonals that can be won by the bot
- A score is given based on the number of cells occupied in one row/column/diagonal.
    If number of cells occupied are 1 then score = 3
                                    2 then score = 9
                                    3 then score = 27
- If a block is won, the given score = 100
- In a similar fashion, calculate the block score for the opponent

#### Board Score
- Multiply all individual block score in a line. This gives probability of winning in each row, column or diagonal.
- In a similar fashion, calculate the block score for the opponent
- We assign BoardScore = OurScore – OpponentScore


## Behaviour
- The bot tries to play the game such that it has its own advantage in a few blocks
while efficiently redirecting the opponent in different blocks such that it will not
have an advantage.
- The occupied blocks are such that a winning position is achieved within a few
moves.
- Then when opponent tries to place two or three marks in a line, the bot would pick up
another favourable line on which it can capture the blocks to win. This is achieved
since the bot has already taken a starting advantage.
- If the opponent is able to handle such behaviour and plays aggressively, the bot
would play defensive and then try to get the winning line.
- To tweak the behaviour, we can adjust the parameters for block and board
evaluation. If the block score parameters are set to 4, 16, 64 or 100 for one, two,
three or four marks in a line respectively, then the bot plays aggressively. However, this
results in draw when playing against highly defensive opponents. With others, it
would still result in comfortable wins.


## Tournament Statistics

|                 | **Total games** | **Win** | **Draw** | **Loss** | **Invalid move** |
|-----------------|-----------------|---------|----------|----------|------------------|
| **Pool Stage**  |     16          |   14    |   2      |     0    |        0         |    
| **Semi-Finals** |     18          |   14    |   3      |     0    |        1         |    
| **Finals**      |     22          |   10    |   6      |     2    |        4         |    